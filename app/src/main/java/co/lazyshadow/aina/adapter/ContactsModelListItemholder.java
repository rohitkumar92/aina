package co.lazyshadow.aina.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import co.lazyshadow.aina.R;
import co.lazyshadow.aina.utils.GlobalUtils;


/**
 * Created by lavadev3 on 17/7/15.
 */
public class ContactsModelListItemholder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private GlobalUtils.ItemClikListener mListener;
    TextView contactName;
    TextView contactNumber;
    ImageView contactImage;

    public ContactsModelListItemholder(View itemView, GlobalUtils.ItemClikListener listener) {
        super(itemView);

        contactName = (TextView) itemView.findViewById(R.id.contactName);
        contactImage = (ImageView) itemView.findViewById(R.id.contactsImage);
        contactNumber = (TextView) itemView.findViewById(R.id.contactNumber);

        /*add = (FloatingActionButton) itemView.findViewById(R.id.fab_add);
        if (add != null){
            add.setTag("add");
            add.setOnClickListener(this);
        }*/
        mListener = listener;
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
      /*  if (mListener != null) {
            if(v instanceof FloatingActionButton && v.getTag().equals("add") ) {
                mListener.onItemClick(v, GlobalUtils.TYPE_ADD, getAdapterPosition());
            }
            else {*/
                mListener.onItemClick(v, getItemViewType(), getAdapterPosition());
            /*}*/

        }
    
}
