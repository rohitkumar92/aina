package co.lazyshadow.aina.adapter;

/**
 * Created by Rohit on 10-05-2016.
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import co.lazyshadow.aina.R;
import co.lazyshadow.aina.model.Contacts;
import co.lazyshadow.aina.utils.GlobalUtils;

public class ContactsModelListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private GlobalUtils.ItemClikListener mItemClikListener;

    private final Context context;
    private List<Contacts> contactsList;
    private ArrayList<Contacts> arraylist = new ArrayList<>();
   // private final String[] clinicName;
    public ContactsModelListAdapter(Context mContext, List<Contacts> contactsList) {
        this.context = mContext;
        this.contactsList = contactsList;
        arraylist.addAll(contactsList);
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.contacts_model_list_item, parent, false);

        ContactsModelListItemholder vh = new ContactsModelListItemholder(v, mItemClikListener);

        return vh;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ContactsModelListItemholder) {

            ((ContactsModelListItemholder) holder).contactName.setText(contactsList.get(position).getContactName());
            ((ContactsModelListItemholder) holder).contactNumber.setText(contactsList.get(position).getContactNumber());
           /* if (contactsList.get(position).getContactImage() != null){
                ((ContactsModelListItemholder) holder).contactImage.setImageBitmap(contactsList.get(position).getContactImage());
            } else {*/
            ((ContactsModelListItemholder) holder).contactImage.setImageDrawable(context.getResources().getDrawable(R.drawable.ellipse));
            //}

           // ((HomeFragmentListItemholder) holder).clinicName.setText(clinicName[position]);
            //Picasso.with(context).load(contactsList.get(position).getContactImage().placeholder(R.drawable.ellipse).into(((ContactsModelListItemholder) holder).contactImage);
        }


    }

    @Override
    public int getItemCount() {
        return contactsList.size();
    }

    public void setOnItemClikListener(GlobalUtils.ItemClikListener listener) {
        this.mItemClikListener = listener;
    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        contactsList.clear();
        if (charText.length() == 0) {
            contactsList.addAll(arraylist);
        } else {
            for (Contacts wp : arraylist) {

                //String name = wp.getManf_name() + wp.getModel_name();
                String name2 = wp.getContactName();

                if (wp.getContactName().toLowerCase(Locale.getDefault())
                        .contains(charText) || wp.getContactName().toLowerCase(Locale.getDefault())
                        .contains(charText) || name2.toLowerCase(Locale.getDefault())
                        .contains(charText)) {
                    contactsList.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }
}
