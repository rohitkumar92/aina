package co.lazyshadow.aina.utils;

/**
 * Created by OdiTek on 03/01/17.
 */

public interface BaseActivity {
    void setToolBarTitle(String name);
}
