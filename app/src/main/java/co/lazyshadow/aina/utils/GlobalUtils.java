package co.lazyshadow.aina.utils;

import android.view.View;

/**
 * Created by lavadev3 on 3/6/15.
 */
public class GlobalUtils {

    public interface ItemClikListener {
        void onItemClick(View view, int itemType, int position);
    }

    public static final int TYPE_EDIT = 1;
    public static final int TYPE_BIKE_ADDED = 2;

    public static final int TYPE_REORDER = 3;
    public static final int TYPE_MODIY = 4;
    public static final int TYPE_CANCEL = 5;
    public static final int TYPE_SELECT = 6;

    public interface ButtonClickListener {
        void onButtonClick(View view, int position);
    }
}
