package co.lazyshadow.aina.activity;

import android.app.ActivityManager;
import android.app.usage.UsageStats;
import android.app.usage.UsageStatsManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.LogOutCallback;
import com.parse.ParseException;
import com.parse.ParseUser;

import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

import co.lazyshadow.aina.R;
import co.lazyshadow.aina.fragment.Account;
import co.lazyshadow.aina.fragment.HomeFragment;
import co.lazyshadow.aina.utils.BaseActivity;
import co.lazyshadow.aina.utils.ChangeDrawer;

public class HomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, BaseActivity, ChangeDrawer {

    private Fragment viewFragment;
    FragmentManager mFragmentManager;
    FragmentTransaction mFragmentTransaction;
    TextView nav_username,nav_email;
    Toolbar toolbar;
    DrawerLayout drawer;
    private int mCurNavigationId;
    private long lastPress;
    ActionBarDrawerToggle toggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        SharedPreferences sharedPreferences = getSharedPreferences("signin", this.MODE_PRIVATE);

        if (sharedPreferences.getString("email","").equals("")){
           startActivity(new Intent(this,SignIn.class));
        }

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("AINA");
        setSupportActionBar(toolbar);

        mCurNavigationId = R.id.home;


        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setItemIconTintList(null);

        View hView =  navigationView.getHeaderView(0);

        nav_username = (TextView) hView.findViewById(R.id.nav_username);
        nav_username.setText("Rohit Kumar");
        Typeface custom_font = Typeface.createFromAsset(getAssets(),  "fonts/GeosansLight.ttf");
        nav_username.setTypeface(custom_font);


        mFragmentManager = getSupportFragmentManager();
        mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.replace(R.id.containerView, new HomeFragment()).commit();


    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        int id = item.getItemId();

        FragmentManager mFragmentManager = getSupportFragmentManager();
        if (id == R.id.home) {
            mCurNavigationId = id;
            viewFragment = new HomeFragment();
            FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.containerView,viewFragment).commit();
        } else if(id == R.id.uberAccount){

        } else if(id == R.id.overrides){

            startActivity(new Intent(HomeActivity.this,OverrideContact.class));
            HomeActivity.this.finish();

        } else if(id == R.id.blacklist){

        } else if(id == R.id.account){

            mCurNavigationId = id;
            viewFragment = new Account();
            FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.containerView,viewFragment).commit();

        } else if(id == R.id.help){

            startActivity(new Intent(HomeActivity.this,HelpActivity.class));
            HomeActivity.this.finish();

        } else if(id == R.id.logout){
            new AlertDialog.Builder(this)
                    .setIcon(getResources().getDrawable(R.drawable.ic_warning_white_24dp))
                    .setTitle("Logout")
                    .setMessage("Are you sure you want to Logout?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            ParseUser.logOutInBackground(new LogOutCallback() {
                                @Override
                                public void done(ParseException e) {
                                    if(e == null) {
                                        Log.i("LOGOUT", "DONE");
                                        SharedPreferences preferences = getSharedPreferences("signin", HomeActivity.this.MODE_PRIVATE);
                                        SharedPreferences.Editor editor = preferences.edit();
                                        editor.putString("email", "");
                                        editor.putString("mobile", "");
                                        editor.apply();
                                        Intent intent = new Intent(getApplicationContext(), SignIn.class);
                                        startActivity(intent);
                                        finish();
                                    }
                                }
                            });


                        }

                    })
                    .setNegativeButton("No", null)
                    .show();
        }
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    protected void closeNavDrawer() {
        if (drawer != null) {
            drawer.closeDrawer(GravityCompat.START);
        }
    }

    protected boolean isNavDrawerOpen() {
        return drawer != null && drawer.isDrawerOpen(GravityCompat.START);
    }

    @Override
    public void setToolBarTitle(String name) {
        toolbar.setTitle(name);
    }

    private void showSnackMsg(String msg) {
        Snackbar snack = Snackbar.make(HomeActivity.this.findViewById(R.id.main_content),
                msg, Snackbar.LENGTH_SHORT);
        View snackView = snack.getView();
        TextView t = (TextView) snackView.findViewById(android.support.design.R.id.snackbar_text);
        t.setTextColor(getResources().getColor(R.color.white));
        snack.show();
    }

    @Override
    public void onBackPressed() {
        if (isNavDrawerOpen()) {
            closeNavDrawer();
        } else {
            if(mCurNavigationId != R.id.home) {
                viewFragment = new HomeFragment();
                FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.containerView,viewFragment).commit();
            } else {
                long currentTime = System.currentTimeMillis();
                if (currentTime - lastPress > 2000) {
                    showSnackMsg(getString(R.string.confirm_exit));
                    lastPress = currentTime;
                } else {
                    super.onBackPressed();
                }
            }
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                startActivity(new Intent(HomeActivity.this, HomeActivity.class));
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setDrawer(boolean value) {
        if (value){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            toggle.setDrawerIndicatorEnabled(false);
            toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    startActivity(new Intent(HomeActivity.this,HomeActivity.class));
                    HomeActivity.this.finish();
                }
            });
        } else {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setDisplayShowHomeEnabled(false);
            toggle.setDrawerIndicatorEnabled(true);
        }
    }




}
