package co.lazyshadow.aina.activity;

import android.animation.ArgbEvaluator;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;

import co.lazyshadow.aina.R;
import co.lazyshadow.aina.fragment.SplashFragmentOne;
import co.lazyshadow.aina.fragment.SplashFragmentTwo;
import co.lazyshadow.aina.utils.Prefs;
import me.relex.circleindicator.CircleIndicator;

public class SplashScreen extends AppCompatActivity implements SplashFragmentOne.OnFragmentInteractionListener,
        SplashFragmentTwo.OnFragmentInteractionListener, View.OnClickListener{

    private final static String TAG = "SplashActivity";
    private final int NUM_SCREENS = 2;

    private static final int REQ_CODE = 23465;

    private View mSplashRoot;
    private ViewPager mViewPager;
    private FragmentPagerAdapter mPagerAdapter;

    private Fragment f1, f2, f3, f4;

    private Button mSkip, mDone, mNext;
    private FrameLayout mNextContainer;
    ArgbEvaluator mArgbEvaluator;

    Integer[] mColors;

    CircleIndicator mPageIndicator;
    //Items from Main activity - early loading
    Thread mDataBootstrapThread = null;
    private BroadcastReceiver mUserCreatedBroadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(!showSplashScreen()) {
            launchMainActivity();
            finish();
        } else {
            setContentView(R.layout.activity_splash_screen);

            setUpColors();

            mSplashRoot = findViewById(R.id.splash_root);

            mViewPager = (ViewPager)findViewById(R.id.splash_pager);
            mPageIndicator = (CircleIndicator) findViewById(R.id.indicator);
            mPagerAdapter = new MySplashPagerAdapter(getSupportFragmentManager());
            mViewPager.setAdapter(mPagerAdapter);
            mPageIndicator.setViewPager(mViewPager);
            mPageIndicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                    if(position < (mPagerAdapter.getCount() - 1) && position < (mColors.length - 1)) {
                        mSplashRoot.setBackgroundColor((Integer)
                                mArgbEvaluator.evaluate(positionOffset, mColors[position], mColors[position + 1]));
                    } else {
                        mSplashRoot.setBackgroundColor(mColors[mColors.length - 1]);
                    }
                }

                @Override
                public void onPageSelected(int position) {
                    Log.d(TAG, "splash screen changed to page #" + position);
                    switch(position) {
                        case 0:
                            splashScreenOneToThree();
                            break;
                        case 1:
                            splashScreenFour();
                            break;
                    }
                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });

            mSkip = (Button) findViewById(R.id.splash_skip);
            mSkip.setTag("skip");
            mSkip.setOnClickListener(this);
            mDone = (Button) findViewById(R.id.splash_done);
            mDone.setTag("done");
            mDone.setOnClickListener(this);
            mNext = (Button) findViewById(R.id.splash_next);
            mNext.setTag("next");
            mNext.setOnClickListener(this);
            mNextContainer = (FrameLayout) findViewById(R.id.splash_next_container);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onClick(View v) {
        if(v instanceof Button && v.getTag().equals("skip")) {
            handleSkip();
        } else if(v instanceof Button && v.getTag().equals("done")) {
            handleSkip();
        } else if(v instanceof Button && v.getTag().equals("next")) {
            int page = mViewPager.getCurrentItem();
            mViewPager.setCurrentItem(page+1);

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == REQ_CODE) {
            if (f1 != null) {
                getSupportFragmentManager().beginTransaction().remove(f1).commit();
                f1 = null;
            }
            if (f2 != null) {
                getSupportFragmentManager().beginTransaction().remove(f2).commit();
                f2 = null;
            }
            finish();
        }
    }

    private void setUpColors(){
        Integer color1 = getResources().getColor(R.color.splash_screen_color_1);
        Integer color2 = getResources().getColor(R.color.splash_screen_color_2);
        Integer color3 = getResources().getColor(R.color.splash_screen_color_3);
        Integer color4 = getResources().getColor(R.color.splash_screen_color_4);

        Integer[] colors_temp = {color1, color2, color3, color4};
        mColors = colors_temp;
        mArgbEvaluator = new ArgbEvaluator();
    }

    private void handleSkip() {
        //Remember that splash screen is not to be shown any more
        Prefs.setSplashScreenPref(this, true);

        //Launch the main activity
        launchMainActivity();

        //Get rid of the fragments
/*        if (f1 != null) {
            getSupportFragmentManager().beginTransaction().remove(f1).commit();
            f1 = null;
        }
        if (f2 != null) {
            getSupportFragmentManager().beginTransaction().remove(f2).commit();
            f2 = null;
        }
        if (f3 != null) {
            getSupportFragmentManager().beginTransaction().remove(f3).commit();
            f3 = null;
        }
        if (f4 != null) {
            getSupportFragmentManager().beginTransaction().remove(f4).commit();
            f4 = null;
        }*/

        //End this activity
        //finish();
    }

    private void launchMainActivity() {
        Intent i = new Intent(this, HomeActivity.class);
        //i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivityForResult(i, REQ_CODE);
    }

    private boolean showSplashScreen() {
        if(Prefs.getSplashScreenPref(this, false)) {
            return false;
        } else {
            return true;
        }
    }

    private void splashScreenOneToThree() {
        if (mSkip != null) {
            mSkip.setVisibility(View.VISIBLE);
        }
        if (mNextContainer != null) {
            mNextContainer.setVisibility(View.VISIBLE);
        }
        if (mDone != null) {
            mDone.setVisibility(View.GONE);
        }
    }

    private void splashScreenFour() {
        if (mSkip != null) {
            mSkip.setVisibility(View.INVISIBLE);
        }
        if (mNextContainer != null) {
            mNextContainer.setVisibility(View.GONE);
        }
        if (mDone != null) {
            mDone.setVisibility(View.VISIBLE);
        }
    }

    private class MySplashPagerAdapter extends FragmentPagerAdapter {

        public MySplashPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            Fragment createdFragment = (Fragment) super.instantiateItem(container, position);
            Log.d(TAG, "instantiateItem " + position);
            switch (position) {
                case 0:
                    f1 = createdFragment;
                    break;
                case 1:
                    f2 = createdFragment;
                    break;
            }
            return createdFragment;
        }

        @Override
        public int getItemPosition(Object object) {

            Log.d(TAG, "getItemPosition");
            return POSITION_NONE; //To make notifyDataSetChanged() do something
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = null;
            Log.d(TAG, "getItem " + position);
            switch(position) {
                case 0:
                    fragment = SplashFragmentOne.newInstance(null, null);
                    break;
                case 1:
                    fragment = SplashFragmentTwo.newInstance(null, null);
                    break;
            }

            return fragment;
        }

        @Override
        public int getCount() {
            return NUM_SCREENS;
        }
    }

    //From MainActivity - early loading
    /*private void collectDeviceData() {
        Prefs.setDeviceId(this.getApplicationContext(), Build.MODEL);
        Prefs.setManufacturer(this.getApplicationContext(), Build.MANUFACTURER);
        TelephonyManager telephony = (TelephonyManager) this.getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE);
        Prefs.setImeiId(this.getApplicationContext(), telephony.getDeviceId());
        Prefs.setAndroidId(this.getApplicationContext(), Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID));
        Prefs.markDeviceDataBootstrapDone(this.getApplicationContext());
    }

    private List<Account> getAccountList() {
        AccountManager am = AccountManager.get(this);
        Account[] accountArray = am.getAccountsByType(GoogleAuthUtil.GOOGLE_ACCOUNT_TYPE);
        String account_list = " ";
        int i;
        List<Account> accounts = new ArrayList<Account>(Arrays.asList(accountArray));
        if (accounts.size() != 0) {
            AccountUtils.setActiveAccount(this, accounts.get(0).name);
        }
        for (i = 0; i < accounts.size(); i++) {
            account_list = account_list + accounts.get(i).name;
            if (i < accounts.size() - 1)
                account_list = account_list + "/";
        }
        if (accounts.size() != 0) {
            AccountUtils.setActiveAccountList(this, account_list);
            return null;
        }
        return accounts;
    }

    *//* Broadcast to generate the auth token for User *//*
    public class UserRegistrations extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (Prefs.hasUserIdGenerated(context)) {
                if (TextUtils.isEmpty(CommonPrefs.getPrefUserToken(context))) {
                    mUser.getAuthToken();
                }
                mUser.updateGCMToken();
                if (!PrefUtils.isDataBootstrapDone(context) && mDataBootstrapThread == null) {
                    Log.d(TAG, "One-time data bootstrap not done yet. Doing now.");
                    performDataBootstrap();
                }

            }
        }
    }

    private void performDataBootstrap() {
        final Context appContext = getApplicationContext();
        Log.d(TAG, "Starting data bootstrap background thread.");
        mDataBootstrapThread = new Thread(new Runnable() {
            @Override
            public void run() {
                *//* First time load will happen here *//*
                Log.d(TAG, "Starting data bootstrap process.");
                PrefUtils.markSyncSucceededNow(appContext);
                PrefUtils.markDataBootstrapDone(appContext);
                mDataBootstrapThread = null;
                // Request a manual sync immediately after the bootstrapping process, in case we
                // have an active connection. Otherwise, the scheduled sync could take a while.
                SyncHelper.requestManualSync(AccountUtils.getActiveAccount(appContext));
                mUser.LoadDeviceInfo(Prefs.getDeviceId(appContext));
            }
        });
        mDataBootstrapThread.start();
    }*/

}
