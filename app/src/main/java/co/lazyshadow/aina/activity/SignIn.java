package co.lazyshadow.aina.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.LogInCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseUser;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import co.lazyshadow.aina.R;
import co.lazyshadow.aina.utils.Prefs;

public class SignIn extends AppCompatActivity {

    TextView textSignIn;

    EditText email;
    EditText phoneNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if(showSplashScreen()) {
            launchSplashActivity();

            super.onCreate(savedInstanceState);
            finish();
            return;
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        textSignIn = (TextView) findViewById(R.id.textSignIn);
        Typeface custom_font = Typeface.createFromAsset(getAssets(),  "fonts/GeosansLight.ttf");
        textSignIn.setTypeface(custom_font);

        email = (EditText) findViewById(R.id.email);
        phoneNumber = (EditText) findViewById(R.id.phoneNumber);




    }

    public void doSignIn(View view) {
        if (isValidMail(email.getText().toString())){

            ParseUser.logInInBackground(email.getText().toString(), phoneNumber.getText().toString(), new LogInCallback() {
                @Override
                public void done(ParseUser user, ParseException e) {
                    if(e == null) {
                        SharedPreferences preferences = getSharedPreferences("signin", SignIn.this.MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putString("email", email.getText().toString());
                        editor.putString("mobile", phoneNumber.getText().toString());
                        editor.putString("objectid", user.getObjectId());
                        editor.apply();
                        startActivity(new Intent(SignIn.this, HomeActivity.class));

                        Log.i("LOGIN", "SUCCESS");
                    }
                    else {
                        Log.i("LOGIN", "FAILED: " + e.getMessage());
                    }
                }
            });
        }

    }

    public void doSignUp(View view) {

        startActivity(new Intent(this, SignUp.class));

    }

    private void launchSplashActivity() {
        Intent i = new Intent(this, SplashScreen.class);
        //i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
    }

    private boolean showSplashScreen() {
        if (Prefs.getSplashScreenPref(this, false)) {
            return false;
        } else {
            return true;
        }
    }

    private boolean isValidMail(String email) {
        boolean check;
        Pattern p;
        Matcher m;

        String EMAIL_STRING = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        p = Pattern.compile(EMAIL_STRING);

        m = p.matcher(email);
        check = m.matches();

        if(!check) {
            Toast.makeText(this,"Please enter current email id",Toast.LENGTH_SHORT).show();
        }
        return check;
    }

    private boolean isValidMobile(String phone) {
        boolean check=false;
        if(!Pattern.matches("[a-zA-Z]+^[+]?[0-9]{10,13}$", phone)){
            if(phone.length() < 10 || phone.length() > 10) {
                // if(phone.length() != 10) {
                check = false;
                Toast.makeText(this,"Not Valid Number",Toast.LENGTH_SHORT).show();
            } else {
                check = true;
            }
        } else {
            check=false;
        }
        return check;
    }
}
