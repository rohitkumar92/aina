package co.lazyshadow.aina.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.vision.text.Text;
import com.parse.ParseObject;

import co.lazyshadow.aina.R;

public class AddAddress extends AppCompatActivity {

    String title, address;
    double latitude,longitude;
    ParseObject parseObject;

    EditText addressText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_address);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarActivity);
        toolbar.setTitle("Add Address");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        addressText = (EditText) findViewById(R.id.addressText);


        Intent intent = getIntent();
        latitude = intent.getDoubleExtra("latitude", 0d);
        longitude = intent.getDoubleExtra("longitude",0d);
        title = intent.getStringExtra("name");
        address = intent.getStringExtra("address");





    }

    public void saveAddress(View view) {

        if (!addressText.getText().toString().isEmpty()){
            parseObject = new ParseObject("Location");
            parseObject.put("shortName",addressText.getText().toString());
            parseObject.put("title",title);
            parseObject.put("address", address);
            parseObject.put("latitude",latitude);
            parseObject.put("longitude",longitude);

            Intent addressIntent = new Intent(AddAddress.this,HomeActivity.class);
            addressIntent.putExtra("shortName",addressText.getText().toString());
            addressIntent.putExtra("latitude",latitude);
            addressIntent.putExtra("longitude",longitude);
            addressIntent.putExtra("name",title);
            addressIntent.putExtra("address",address);
            startActivity(addressIntent);

        } else {
            Toast.makeText(AddAddress.this,"Please enter name to save", Toast.LENGTH_SHORT).show();
        }



    }
}
