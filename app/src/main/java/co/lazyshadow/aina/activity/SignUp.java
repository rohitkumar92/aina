package co.lazyshadow.aina.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatEditText;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.LogOutCallback;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

import co.lazyshadow.aina.R;

public class SignUp extends AppCompatActivity {

    TextView textSignUp;
    AppCompatEditText email, password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        textSignUp = (TextView) findViewById(R.id.textSignUp);
        email = (AppCompatEditText) findViewById(R.id.email);
        password = (AppCompatEditText) findViewById(R.id.phoneNumber);

        Typeface custom_font = Typeface.createFromAsset(getAssets(),  "fonts/GeosansLight.ttf");
        textSignUp.setTypeface(custom_font);
    }

    public void doSignUp(View view) {

        final ParseUser user = new ParseUser();
        user.setUsername(email.getText().toString());
        user.setPassword(password.getText().toString());
        user.setEmail(email.getText().toString());



        user.signUpInBackground(new SignUpCallback() {
            @Override
            public void done(ParseException e) {
                if(e == null) {
                    startActivity(new Intent(SignUp.this, HomeActivity.class));
                    Log.i("SignUp", "SUCCESS: " + user.getObjectId());
                }
                else {
                    Log.i("SignUp", "FAILED: "+e.getMessage());
                    Toast.makeText(SignUp.this, "SignUp Failed", Toast.LENGTH_SHORT).show();
                }
            }
        });


    }
}
