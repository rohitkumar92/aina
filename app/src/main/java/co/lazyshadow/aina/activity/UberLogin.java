package co.lazyshadow.aina.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import co.lazyshadow.aina.R;

public class UberLogin extends AppCompatActivity {

    TextView signInToUber;
    TextView uberAccountSignIN;
    TextView emailUber;
    TextView skipForNow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_uber_login);

        signInToUber = (TextView) findViewById(R.id.signInToUber);
        uberAccountSignIN = (TextView) findViewById(R.id.uberAccountSignIN);
        emailUber = (TextView) findViewById(R.id.emailUber);
        skipForNow = (TextView) findViewById(R.id.skipForNow);

        Typeface custom_font = Typeface.createFromAsset(getAssets(),  "fonts/GeosansLight.ttf");

        signInToUber.setTypeface(custom_font);
        uberAccountSignIN.setTypeface(custom_font);
        emailUber.setTypeface(custom_font);
        skipForNow.setTypeface(custom_font);

    }

    public void doUberSignIn(View view) {
        startActivity(new Intent(UberLogin.this,HomeActivity.class));
    }

    public void doUberSignUp(View view) {
        startActivity(new Intent(UberLogin.this,HomeActivity.class));
    }
}
