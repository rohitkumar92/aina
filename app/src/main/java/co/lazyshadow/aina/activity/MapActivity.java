package co.lazyshadow.aina.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.parse.ParseObject;

import java.util.Map;

import co.lazyshadow.aina.R;

public class MapActivity extends AppCompatActivity implements OnMapReadyCallback {

    private LinearLayout searchPlace;
    String title, address;
    double latitude,longitude;
    private Marker myMarker;
    GoogleMap mMap;

    ParseObject parseObject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);




        Intent intent = getIntent();
        latitude = intent.getDoubleExtra("latitude", 0d);
        longitude = intent.getDoubleExtra("longitude",0d);
        title = intent.getStringExtra("name");
        address = intent.getStringExtra("address");

        SupportMapFragment mapFragment = (SupportMapFragment)getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


    }

    public void onMapReady(GoogleMap map){



        mMap = map;
        LatLng sydney = new LatLng(latitude,longitude);


        map.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 13));
        map.addMarker( new MarkerOptions()
                .title(title)
                .snippet(address)
                .position(sydney));

        map.setMyLocationEnabled(true);

        map.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                Log.e("Marker",title);
                Intent intent = new Intent(MapActivity.this, AddAddress.class);
                intent.putExtra("latitude",latitude);
                intent.putExtra("longitude",longitude);
                intent.putExtra("name",title);
                intent.putExtra("address",address);
                startActivity(intent);
                /*parseObject = new ParseObject("Location");
                parseObject.put("address", title);
                parseObject.put("latitude",latitude);
                parseObject.put("longitude",longitude);*/

            }
        });
    }
}
