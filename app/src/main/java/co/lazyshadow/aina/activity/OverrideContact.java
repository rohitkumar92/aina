package co.lazyshadow.aina.activity;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import co.lazyshadow.aina.R;
import co.lazyshadow.aina.adapter.ContactsModelListAdapter;
import co.lazyshadow.aina.model.Contacts;
import co.lazyshadow.aina.utils.GlobalUtils;

public class OverrideContact extends AppCompatActivity implements GlobalUtils.ItemClikListener {

    Contacts contacts;
    List<Contacts> contactsList;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private ContactsModelListAdapter contactsModelListAdapter;
    private SearchView searchView;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_override_contact);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarActivity);
        toolbar.setTitle("Override Contact");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        progressBar = (ProgressBar) findViewById(R.id.progressbar);

        mRecyclerView = (RecyclerView) findViewById(R.id.contacts_recycler);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        searchView = (android.support.v7.widget.SearchView) findViewById(R.id.search);
        searchView.setQueryHint("Enter query to search");
        searchView.onActionViewExpanded();
        searchView.setIconified(false);
        searchView.clearFocus();
        LoadContact loadContact = new LoadContact();
        loadContact.execute();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                //bookingListAdapter.filter(s);
                return false;
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                contactsModelListAdapter.filter(newText);
                return false;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                startActivity(new Intent(OverrideContact.this, HomeActivity.class));
                OverrideContact.this.finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private class LoadContact extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);

        }

        @Override
        protected Void doInBackground(Void... voids) {
            // Get Contact list from Phone

            ContentResolver cr = getContentResolver();
            Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,
                    null, null, null, null);

            if ((cur != null ? cur.getCount() : 0) > 0) {
                contactsList = new ArrayList<>();
                while (cur != null && cur.moveToNext()) {
                    String id = cur.getString(
                            cur.getColumnIndex(ContactsContract.Contacts._ID));
                    String name = cur.getString(cur.getColumnIndex(
                            ContactsContract.Contacts.DISPLAY_NAME));

                    if (cur.getInt(cur.getColumnIndex(
                            ContactsContract.Contacts.HAS_PHONE_NUMBER)) > 0) {
                        Cursor pCur = cr.query(
                                ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                                null,
                                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                                new String[]{id}, null);

                        while (pCur.moveToNext()) {

                            String phoneNo = pCur.getString(pCur.getColumnIndex(
                                    ContactsContract.CommonDataKinds.Phone.NUMBER));
                            contacts = new Contacts();
                            contacts.setContactName(name);
                            contacts.setContactNumber(phoneNo);
                            contacts.setContactImage("");
                            contactsList.add(contacts);
                            Log.e("Override Contact", "Name: " + name);
                            Log.e("Override Contact", "Phone Number: " + phoneNo);
                        }
                        pCur.close();
                    }
                }



            }
            if(cur!=null){
                cur.close();
            }
            //phones.close();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progressBar.setVisibility(View.GONE);
            contactsModelListAdapter = new ContactsModelListAdapter(OverrideContact.this,contactsList);
            mRecyclerView.setAdapter(contactsModelListAdapter);
            contactsModelListAdapter.setOnItemClikListener(OverrideContact.this);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onItemClick(View view, int itemType, int position) {

        SharedPreferences preferences = getSharedPreferences("overrideContact", MODE_PRIVATE);
        String user_pic = preferences.getString("user_pic","");
        if (user_pic.equals("user_pic_1")){
            preferences = getSharedPreferences("overrideContactName1", MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString("user_name_1",contactsList.get(position).getContactName());
            editor.putString("user_number_1",contactsList.get(position).getContactNumber());
            editor.apply();
        }
        if (user_pic.equals("user_pic_2")){
            preferences = getSharedPreferences("overrideContactName2", MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString("user_name_2",contactsList.get(position).getContactName());
            editor.putString("user_number_2",contactsList.get(position).getContactNumber());
            editor.apply();
        }

        startActivity(new Intent(this, HomeActivity.class));

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this,HomeActivity.class));
        OverrideContact.this.finish();
    }
}
