package co.lazyshadow.aina.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import co.lazyshadow.aina.R;
import co.lazyshadow.aina.utils.BaseActivity;


public class Account extends Fragment {

    private BaseActivity setToolBarTitle;


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getActivity().setTitle("HOME");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //return inflater.inflate(R.layout.current_appointments, container, false);
        View view = inflater.inflate(R.layout.account, container, false);
        //initialiseView(view, this.getActivity());
        setToolBarTitle = (BaseActivity) getActivity();
        setToolBarTitle.setToolBarTitle("Home");

        return view;
    }
}
