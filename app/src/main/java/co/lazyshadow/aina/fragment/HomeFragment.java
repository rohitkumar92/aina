package co.lazyshadow.aina.fragment;

import android.app.ActivityManager;
import android.app.usage.UsageStats;
import android.app.usage.UsageStatsManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.CallLog;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

import co.lazyshadow.aina.R;
import co.lazyshadow.aina.activity.HomeActivity;
import co.lazyshadow.aina.activity.OverrideContact;
import co.lazyshadow.aina.activity.SelectAddress;
import co.lazyshadow.aina.services.LogReceiver;
import co.lazyshadow.aina.utils.BaseActivity;
import co.lazyshadow.aina.utils.ChangeDrawer;
import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.Context.ACTIVITY_SERVICE;


public class HomeFragment extends Fragment { //implements FragmentManager.OnBackStackChangedListener

    BaseActivity setToolBarTitle;
    ChangeDrawer changeDrawer;
    CircleImageView user_pic_imv, user_pic_imv_1,user_pic_imv2;
    TextView user_name1, user_name2;
    TextView location,uberRide;

    String title="", address="",shortName="";
    double latitude,longitude;

    HomeActivity homeActivity;

    boolean userClicked = false;

    UsageStatsManager mUsageStatsManager;

    ArrayList<String> appList;

    Bundle bundle;

    BroadcastReceiver callPredBroadcast;
    ArrayList<String> predCalls = null;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        homeActivity = (HomeActivity) getActivity();

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //return inflater.inflate(R.layout.current_appointments, container, false);
        View view = inflater.inflate(R.layout.homefragment, container, false);
        initialiseView(view, this.getActivity());

        setHasOptionsMenu(false);

        setToolBarTitle = (BaseActivity) getActivity();
        setToolBarTitle.setToolBarTitle("AINA");

        //getProcessName();

        changeDrawer = (ChangeDrawer) getActivity();

        try{
            bundle = new Bundle();
            bundle = getActivity().getIntent().getExtras();
            if (!bundle.isEmpty()){
                shortName = bundle.getString("shortName");
                title = bundle.getString("title");
                address = bundle.getString("address");
                latitude = bundle.getDouble("latitude");
                longitude = bundle.getDouble("longitude");
            }
        } catch (Exception e){
            e.printStackTrace();
        }



        SharedPreferences preferences = getActivity().getSharedPreferences("homeFragment", getActivity().MODE_PRIVATE);
        if (preferences.getString("usage_stat", "").equals("false") || preferences.getString("usage_stat", "").equals("")) {
            AlertDialog.Builder builder;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                builder = new AlertDialog.Builder(getActivity(), android.R.style.Theme_Material_Dialog_Alert);
            } else {
                builder = new AlertDialog.Builder(getActivity());
            }
            builder.setTitle("Access Usage")
                    .setMessage("AINA would like to acces your usage stats")
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            SharedPreferences preferences = getActivity().getSharedPreferences("homeFragment", getActivity().MODE_PRIVATE);
                            SharedPreferences.Editor editor = preferences.edit();
                            editor.putString("usage_stat", "true");
                            editor.apply();
                            getActivity().startActivity(new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS));
                        }
                    })
                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            SharedPreferences preferences = getActivity().getSharedPreferences("overrideContact", getActivity().MODE_PRIVATE);
                            SharedPreferences.Editor editor = preferences.edit();
                            editor.putString("usage_stat", "false");
                            editor.apply();
                            dialog.dismiss();
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        }

        getActivity().startService(new Intent(getActivity(),LogReceiver.class));
        Log.i("SERVICE", "STARTED");


        return view;
    }

    public String getContactName(final String phoneNumber, Context context)
    {
        Uri uri=Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI,Uri.encode(phoneNumber));

        String[] projection = new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME};

        String contactName="";
        Cursor cursor=context.getContentResolver().query(uri,projection,null,null,null);

        if (cursor != null) {
            if(cursor.moveToFirst()) {
                contactName=cursor.getString(0);
            }
            cursor.close();
        }

        return contactName;
    }

    @Override
    public void onResume() {
        super.onResume();

        IntentFilter callPredFilter = new IntentFilter("co.belazy.aina.callPred");

        callPredBroadcast = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                predCalls = intent.getStringArrayListExtra("predCalls");
                Log.i("RECEIVED BROADCAST", predCalls.get(0));

                for(int i = 0;i<predCalls.size(); i++) {
                    Log.i("CALLS" + Integer.toString(i), predCalls.get(i));

                }

                if(predCalls != null) {
                    Calendar cal = Calendar.getInstance();
                    String number1 = predCalls.get(2 * cal.get(Calendar.HOUR_OF_DAY));
                    String number2 = predCalls.get(2 * cal.get(Calendar.HOUR_OF_DAY) + 1);

                    Log.i("NUMBERS", number1 + " " + number2);

                    if(number1.compareTo("NA") != 0)
                        user_name1.setText(getContactName(number1, getActivity()));
                    else
                        user_name1.setText(number1);

                    if(number2.compareTo("NA") != 0)
                        user_name2.setText(getContactName(number2, getActivity()));
                    else
                        user_name2.setText(number2);
                }
            }
        };

        if(predCalls != null) {
            Calendar cal = Calendar.getInstance();
            String number1 = predCalls.get(2 * cal.get(Calendar.HOUR_OF_DAY));
            String number2 = predCalls.get(2 * cal.get(Calendar.HOUR_OF_DAY) + 1);

            if(number1.compareTo("NA") != 0)
                user_name1.setText(getContactName(number1, getActivity()));
            else
                user_name1.setText(number1);

            if(number2.compareTo("NA") != 0)
                user_name2.setText(getContactName(number2, getActivity()));
            else
                user_name2.setText(number2);
        }

        getActivity().registerReceiver(callPredBroadcast, callPredFilter);
    }

    @Override
    public void onPause() {
        super.onPause();

        getActivity().unregisterReceiver(callPredBroadcast);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (userClicked){
            inflater.inflate(R.menu.override, menu);
        } else{
            inflater.inflate(R.menu.work, menu);
        }

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.overrideContact:
                startActivity(new Intent(getActivity(), OverrideContact.class));
                break;
            case R.id.add:
                startActivity(new Intent(getActivity(), SelectAddress.class));
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return super.onOptionsItemSelected(item);
    }

    void initialiseView(View v, final Context ctx) {

        user_name1 = (TextView) v.findViewById(R.id.user_name1);
        user_name2 = (TextView) v.findViewById(R.id.user_name2);
        uberRide = (TextView) v.findViewById(R.id.uberRide);
        location = (TextView) v.findViewById(R.id.location);


        Typeface custom_font = Typeface.createFromAsset(getActivity().getAssets(),  "fonts/GeosansLight.ttf");

        user_name1.setTypeface(custom_font);
        user_name2.setTypeface(custom_font);
        uberRide.setTypeface(custom_font);
        location.setTypeface(custom_font);

        if (!title.isEmpty() || !title.equals("")){
            location.setText(shortName);
        }

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("overrideContactName1", ctx.MODE_PRIVATE);

        if (!sharedPreferences.getString("user_name_1", "").equals("")) {
            user_name1.setText(sharedPreferences.getString("user_name_1", ""));
        }
        sharedPreferences = getActivity().getSharedPreferences("overrideContactName2", ctx.MODE_PRIVATE);
        if (!sharedPreferences.getString("user_name_2", "").equals("")) {
            user_name2.setText(sharedPreferences.getString("user_name_2", ""));
        }

        user_pic_imv = (CircleImageView) v.findViewById(R.id.user_pic_imv);
        user_pic_imv_1 = (CircleImageView) v.findViewById(R.id.user_pic_imv1);
        user_pic_imv2 = (CircleImageView) v.findViewById(R.id.user_pic_imv2);
        user_pic_imv.setOnClickListener(    new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userClicked = true;
                setHasOptionsMenu(true);
                changeDrawer.setDrawer(true);
                setToolBarTitle.setToolBarTitle("");
                SharedPreferences preferences = getActivity().getSharedPreferences("overrideContact", ctx.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString("user_pic", "user_pic_1");
                editor.apply();

            }
        });
        user_pic_imv_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userClicked = true;
                setHasOptionsMenu(true);
                changeDrawer.setDrawer(true);
                setToolBarTitle.setToolBarTitle("");
                SharedPreferences preferences = getActivity().getSharedPreferences("overrideContact", ctx.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString("user_pic", "user_pic_2");
                editor.apply();

            }
        });

        user_pic_imv2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userClicked = false;
                setHasOptionsMenu(true);
                changeDrawer.setDrawer(true);
                setToolBarTitle.setToolBarTitle("");
            }
        });

        sharedPreferences = getActivity().getSharedPreferences("overrideContact", ctx.MODE_PRIVATE);
        String user_pic = sharedPreferences.getString("user_pic", "");
        if (!user_pic.equals("")) {
            if (user_pic.equals("user_pic_1")) {
                sharedPreferences = getActivity().getSharedPreferences("overrideContactName1", ctx.MODE_PRIVATE);
                user_name1.setText(sharedPreferences.getString("user_name_1", ""));
            } else if (user_pic.equals("user_pic_2")) {
                sharedPreferences = getActivity().getSharedPreferences("overrideContactName2", ctx.MODE_PRIVATE);
                user_name2.setText(sharedPreferences.getString("user_name_2", ""));
            }
        }
    }
}
