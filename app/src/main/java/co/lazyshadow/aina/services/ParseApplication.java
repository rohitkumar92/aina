package co.lazyshadow.aina.services;

import android.app.Application;

import com.parse.Parse;

/**
 * Created by rohit on 24/10/17.
 */

public class ParseApplication extends Application {
    public void onCreate()
    {
        super.onCreate();
        Parse.initialize(this);
    }
}
