package co.lazyshadow.aina.services;

/**
 * Created by rohit on 23/10/17.
 */

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.usage.UsageStats;
import android.app.usage.UsageStatsManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.PowerManager;
import android.provider.CallLog;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import co.lazyshadow.aina.model.AppUsageStats;
import co.lazyshadow.aina.model.CallLogList;

public class Alarm extends BroadcastReceiver {

    UsageStatsManager mUsageStatsManager;

    ArrayList<AppUsageStats> appList;

    Context mContext = null;
    String phNumber;
    String callType;
    String callDate;
    Date callDayTime;
    String callDuration;
    String dir;

    List<CallLogList> callLogList;
    CallLogList callLog;
    ParseObject parseObject;

    List<HashMap<String, Integer>> freqCount;


    @SuppressLint("WrongConstant")
    @Override
    public void onReceive(final Context context, Intent intent) {

        mContext = context;


        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "");
        wl.acquire();

        // Put here YOUR code.
        // For example

        mUsageStatsManager = (UsageStatsManager) context
                .getSystemService("usagestats");

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            Toast.makeText(context, "Alarm !!!!!!!!!!", Toast.LENGTH_LONG).show();
            List<UsageStats> usageStatsList = getUsageStatistics(UsageStatsManager.INTERVAL_DAILY);
            Collections.sort(usageStatsList, new LastTimeLaunchedComparatorDesc());
            appList = new ArrayList<>();
            for (int i = 0; i < usageStatsList.size(); i++) {
                String appName = usageStatsList.get(i).getPackageName();
                appList.add(new AppUsageStats(appName, Long.toString(usageStatsList.get(i).getLastTimeUsed()), Long.toString(usageStatsList.get(i).getTotalTimeInForeground())));

            }

            Log.e("App List", appList.toString());
            getCallDetails(context, false);



            for (int i = 0; i < callLogList.size(); i++) {
                parseObject = new ParseObject("CallLogs");
                parseObject.put("userid", ParseUser.getCurrentUser().getObjectId());
                parseObject.put("phoneNumber", callLogList.get(i).getPhoneNumber());
                parseObject.put("callType", callLogList.get(i).getCallType());
                parseObject.put("duration", callLogList.get(i).getDuration());
                parseObject.put("date", callLogList.get(i).getCallDate());
                // parseObject.put("Date",dt);
                Log.e("parseprint", parseObject.toString());
                parseObject.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        if (e != null) {
                            Log.e("Parseobject", e.toString());
                        } else {
                            Log.e("Success", "Sussess");
                        }
                    }
                });

            }


            for (int i = 0; i < appList.size(); i++) {
                parseObject = new ParseObject("Mobile");

                parseObject.put("userid", ParseUser.getCurrentUser().getObjectId());
                parseObject.put("AppPackage", appList.get(i).getAppPackage());
                parseObject.put("LastTimeUsed", appList.get(i).getLastTimeUsed());
                parseObject.put("TimeInForeground", appList.get(i).getTotalTimeInForeground());
                /*parseObject.put("callType",callLogList.get(i).getCallType());
                parseObject.put("duration",callLogList.get(i).getDuration());*/
                // parseObject.put("Date",dt);
                Log.e("parsePrintMobile", parseObject.toString());
                parseObject.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        if (e != null) {
                            Log.e("ParseMobileObject", e.toString());
                        } else {

                            Log.e("MobileSuccess", "Success");
                            cancelAlarm(context);
                        }
                    }
                });

            }


        }

        wl.release();

        computeCallPrediction();
    }

    private void computeCallPrediction() {
        getCallDetails(mContext, true);
        freqCount = new ArrayList<HashMap<String, Integer>>();
        for(int i = 0; i< 24; i++) {
            HashMap<String, Integer> map = new HashMap<String, Integer>();
            freqCount.add(map);
        }
        Calendar cal = Calendar.getInstance();
        for (int i = 0; i < callLogList.size(); i++) {
            if(callLogList.get(i).getCallType() != "OUTGOING")
                continue;
            String phone = callLogList.get(i).getPhoneNumber();
            Log.i("PHONENUM", phone + " " + callLogList.get(i).getCallDate() + " " + Long.parseLong(callLogList.get(i).getCallDate()));
            cal.setTimeInMillis(Long.parseLong(callLogList.get(i).getCallDate()));
            int hrDay = cal.get(cal.HOUR_OF_DAY);
            int count = freqCount.get(hrDay).containsKey(phone) ? freqCount.get(hrDay).get(phone) : 0;
            freqCount.get(hrDay).put(phone, count + 1);
            Log.i("COUNT: " + Integer.toString(hrDay), Integer.toString(count + 1));
        }

        ArrayList<String> predNumbers = new ArrayList<String>();
        for(Integer i = 0; i < 24; i++) {
            Log.i("FREQSET", Integer.toString(freqCount.get(i).size()));
            List<Map.Entry<String, Integer>> list = new LinkedList<Map.Entry<String, Integer>>(freqCount.get(i).entrySet());
            Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
                public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
                    return (o2.getValue()).compareTo(o1.getValue());
                }
            });
            Log.i("LIST SIZE", Integer.toString(list.size()));
            if (list.size() > 1) {
                predNumbers.add(list.get(0).getKey());
                predNumbers.add(list.get(1).getKey());
                Log.i("Predict: " + i.toString(), list.get(0).getKey() + " " + list.get(0).getValue());
            }
            else if(list.size() > 0) {
                predNumbers.add(list.get(0).getKey());
                predNumbers.add("NA");
            }
            else {
                predNumbers.add("NA");
                predNumbers.add("NA");
            }
        }

        Intent i = new Intent("co.belazy.aina.callPred");
        i.putStringArrayListExtra("predCalls", predNumbers);
        mContext.sendBroadcast(i);

    }

    public void setAlarm(Context context) {
        mContext = context;
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, Alarm.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, 0);

        // reset previous pending intent
        /*alarmManager.cancel(pendingIntent);*/

        // Set the alarm to start at approximately 08:00 morning.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.HOUR_OF_DAY, 1);
        calendar.set(Calendar.MINUTE, 28);
        calendar.set(Calendar.SECOND, 0);

        // if the scheduler date is passed, move scheduler time to tomorrow
        if (System.currentTimeMillis() > calendar.getTimeInMillis()) {
            calendar.add(Calendar.DAY_OF_YEAR, 1);
        }

        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
                AlarmManager.INTERVAL_DAY, pendingIntent);

        Log.i("Alarm set for: ", calendar.toString());

        computeCallPrediction();

    }

    public void cancelAlarm(Context context) {
        Intent intent = new Intent(context, Alarm.class);
        PendingIntent sender = PendingIntent.getBroadcast(context, 0, intent, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(sender);
    }

    private static class LastTimeLaunchedComparatorDesc implements Comparator<UsageStats> {

        @Override
        public int compare(UsageStats left, UsageStats right) {
            return Long.compare(right.getLastTimeUsed(), left.getLastTimeUsed());
        }
    }

    public List<UsageStats> getUsageStatistics(int intervalType) {
        // Get the app statistics since one year ago from the current time.
        Calendar cal = Calendar.getInstance();
        SharedPreferences pref = mContext.getSharedPreferences("co.belazy.aina", Context.MODE_PRIVATE);
        if (pref.getBoolean("firstRun_usage", true)) {
            pref.edit().putBoolean("firstRun_usage", false).commit();
            Log.i("RUN", "FIRST_USAGE");
            cal.add(Calendar.YEAR, -1);
        }
        else {
            Log.i("RUN", "NOT FIRST RUN USAGE");

            cal.add(Calendar.DAY_OF_YEAR, -1);
        }

        List<UsageStats> queryUsageStats = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            queryUsageStats = mUsageStatsManager
                    .queryUsageStats(intervalType, cal.getTimeInMillis(),
                            System.currentTimeMillis());
        }

        return queryUsageStats;
    }

    private void getCallDetails(Context context, Boolean full) {


        StringBuffer sb = new StringBuffer();
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_CALL_LOG) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        String selection = null;

        // Check if the app is running for the first time
        SharedPreferences pref = context.getSharedPreferences("co.belazy.aina", Context.MODE_PRIVATE);
        if (pref.getBoolean("firstRun", true)) {
            pref.edit().putBoolean("firstRun", false).commit();
            Log.i("RUN", "FIRST");
        }
        else {
            Calendar cal = GregorianCalendar.getInstance(TimeZone.getTimeZone("Asia/Kolkata"));
            cal.add(Calendar.DAY_OF_YEAR, -1);
            selection = CallLog.Calls.DATE + " > " + cal.getTimeInMillis();
            Log.i("RUN", "NOT FIRST");
        }

        if(full == true) {
            selection = null;
        }

        String[] projection = {CallLog.Calls.NUMBER, CallLog.Calls.TYPE, CallLog.Calls.DATE, CallLog.Calls.DURATION};
        Cursor managedCursor = mContext.getContentResolver().query(CallLog.Calls.CONTENT_URI, projection, selection, null, null);
        int number = 0;
        callLogList = new ArrayList<>();
        if (managedCursor != null) {
            number = managedCursor.getColumnIndex(CallLog.Calls.NUMBER);

            int type = managedCursor.getColumnIndex(CallLog.Calls.TYPE);
            int date = managedCursor.getColumnIndex(CallLog.Calls.DATE);
            int duration = managedCursor.getColumnIndex(CallLog.Calls.DURATION);

            sb.append("Call Details :");
            while (managedCursor.moveToNext()) {
                callLog = new CallLogList();
                phNumber = managedCursor.getString(number);
                callType = managedCursor.getString(type);
                callDate = managedCursor.getString(date);
                callDayTime = new Date(Long.valueOf(callDate));
                callDuration = managedCursor.getString(duration);
                dir = null;
                int dircode = Integer.parseInt(callType);
                switch (dircode) {
                    case CallLog.Calls.OUTGOING_TYPE:
                        dir = "OUTGOING";
                        break;

                    case CallLog.Calls.INCOMING_TYPE:
                        dir = "INCOMING";
                        break;

                    case CallLog.Calls.MISSED_TYPE:
                        dir = "MISSED";
                        break;
                }

                callLog.setCallType(dir);
                callLog.setDuration(callDuration);
                callLog.setPhoneNumber(phNumber);
                callLog.setCallDate(callDate);

                callLogList.add(callLog);


                sb.append("\nPhone Number:--- " + phNumber + " \nCall Type:--- " + dir + " \nCall Date:--- " + callDayTime + " \nCall duration in sec :--- " + callDuration);
                sb.append("\n----------------------------------");

                Log.e("Call Details", "\nPhone Number:--- " + phNumber + " \nCall Type:--- " + dir + " \nCall Date:--- " + callDayTime + " \nCall duration in sec :--- " + callDuration);
            }
        }
        managedCursor.close();
    }
}
