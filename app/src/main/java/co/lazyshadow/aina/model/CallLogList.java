package co.lazyshadow.aina.model;

/**
 * Created by rohit on 24/10/17.
 */

public class CallLogList {

    private String phoneNumber;
    private String callType;
    private String duration;
    private String date;

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getCallType() {
        return callType;
    }

    public void setCallType(String callType) {
        this.callType = callType;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getCallDate() {
        return date;
    }

    public void setCallDate(String date) {
        this.date = date;
    }
}
