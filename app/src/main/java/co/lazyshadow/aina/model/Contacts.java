package co.lazyshadow.aina.model;

/**
 * Created by rohit on 21/10/17.
 */

public class Contacts {

    private String contactName;
    private String contactNumber;
    private String contactImage;

    public String getContactImage() {
        return contactImage;
    }

    public void setContactImage(String contactImage) {
        this.contactImage = contactImage;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }
}
