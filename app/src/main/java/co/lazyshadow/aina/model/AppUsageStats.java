package co.lazyshadow.aina.model;

/**
 * Created by marslab on 11/11/17.
 */

public class AppUsageStats {
    private String _appPackage;
    private String _lastTimeUsed;
    private String _totalTimeInForeground;

    public AppUsageStats(String _appPackage, String _lastTimeUsed, String _totalTimeInForeground) {
        this._appPackage = _appPackage;
        this._lastTimeUsed = _lastTimeUsed;
        this._totalTimeInForeground = _totalTimeInForeground;
    }

    public String getAppPackage() {
        return _appPackage;
    }

    public void setAppPackage(String _appPackage) {
        this._appPackage = _appPackage;
    }

    public String getLastTimeUsed() {
        return _lastTimeUsed;
    }

    public void setLastTimeUsed(String _lastTimeUsed) {
        this._lastTimeUsed = _lastTimeUsed;
    }

    public String getTotalTimeInForeground() {
        return _totalTimeInForeground;
    }

    public void setTotalTimeInForeground(String _totalTimeInForeground) {
        this._totalTimeInForeground = _totalTimeInForeground;
    }
}
